package main

import (
	"fmt"
	"log"
	"os"
	"runtime"

	"gitlab-org/cloud-native/gitlab-logger/internal/config"
	"gitlab-org/cloud-native/gitlab-logger/internal/logger"
)

var (
	version   string
	buildtime string
)

// GetVersionString returns a standard version header
func GetVersionString() string {
	return fmt.Sprintf("gitlab-logger, version %v", version)
}

// GetVersion returns the semver compatible version number
func GetVersion() string {
	return version
}

// GetBuildTime returns the time at which the build took place
func GetBuildTime() string {
	return buildtime
}

func main() {
	cfg := config.New(os.Args[1:])

	if cfg.ShowVersion {
		fmt.Println(GetVersionString())
		os.Exit(0)
	}

	if cfg.DebugMode {
		logger := log.New(os.Stderr, "", 0)
		logger.SetPrefix("DEBUG: ")
		logger.Print("Version: ", GetVersionString())
		logger.Print("Exclude: ", cfg.Exclude)
		logger.Print("Poll: ", cfg.Poll)
		logger.Print("JSON: ", cfg.JSON)
		logger.Print("minLevel: ", cfg.MinLevel)
		logger.Print("pidToWatch: ", cfg.PidToWatch)
		logger.Print("readFromEnd: ", cfg.ReadFromEnd)
		logger.Print("truncateLogs: ", cfg.TruncateLogs)
		logger.Print("truncateLogsInterval: ", cfg.TruncateLogsInterval)
		logger.Print("maxLogFileSize: ", cfg.MaxLogFileSize)
	}

	l := logger.New(cfg)
	l.Start(os.Args[1:])

	runtime.Goexit()
}
