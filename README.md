# Gitlab Logger
This allows to disable tail based Gitlab logging (by providing a no-op tail) and instead use a sidecar logger agent that tail files and output (semi-)structured logs (JSON) on stdout or, eventually, use it as a tail dropin replacement.

Contribution is more than welcomed!

[![GitHub license](https://img.shields.io/github/license/jbguerraz/gitlab-logger.svg?c)](https://github.com/jbguerraz/gitlab-logger/blob/master/LICENSE)
[![GoDoc](https://godoc.org/github.com/jbguerraz/gitlab-logger?status.svg)](https://pkg.go.dev/github.com/jbguerraz/gitlab-logger?tab=doc)
[![Go Report Card](https://goreportcard.com/badge/github.com/jbguerraz/gitlab-logger)](https://goreportcard.com/report/github.com/jbguerraz/gitlab-logger)
[![GitHub issues](https://img.shields.io/github/issues/jbguerraz/gitlab-logger.svg)](https://github.com/jbguerraz/gitlab-logger/issues)

## Configuration

Configuration is done using flags or environment variables. Any flag supplied in the command line will either override or append to existing values set via corresponding environment variables.

Each configuration option is described below. Additionally, you can run the following command to see usage details:

```sh
gitlab-logger -usage
```

When used as a tail drop-in replacement, use a wrapper script if you want to configure it.

For example, in `/usr/local/bin/tail`

```sh
#!/usr/bin/env sh
exec /usr/local/bin/gitlab-logger --poll=true --minlevel=3 $@
```

Alternatively, if you'd like to use environment variables for configuration:

```sh
#!/usr/bin/env sh
export GITLAB_LOGGER_POLL="true"
export GITLAB_LOGGER_MINLEVEL="3"
exec /usr/local/bin/gitlab-logger $@
```

Or mix-and-match flags and environment variables:

```sh
#!/usr/bin/env sh
export GITLAB_LOGGER_MINLEVEL="3"
exec /usr/local/bin/gitlab-logger --poll=true --json=false $@
```

### debug

Set it to true to enable extra verbose logs. All information will be printed to stderr.

default: `false`

Equivalent environment variable: `GITLAB_LOGGER_DEBUG`

### watch-pid

The PID of the process to watch.

default: `0`

Equivalent environment variable: `GITLAB_LOGGER_WATCH_PID`

### poll

Set it to true to use polling instead of inotify for directory watching (used when a directory is passed in the list of files to tail. e.g: `/var/log/gitlab`)

default: `false`

Equivalent environment variable: `GITLAB_LOGGER_POLL`

### json

Set it to true to keep only (json) structured event logs

default: `false`

Equivalent environment variable: `GITLAB_LOGGER_JSON`

### minlevel

Minimum log level, used to filter logs. Level is keyword detection based. Some won't match, so we have `unknown`.

| Value | Level keyword |
| - | :---- |
| 1 | debug
| 2 | info
| 3 | notice
| 4 | warn
| 5 | err
| 6 | fatal
| 100 | unknown

default: `0`

Equivalent environment variable: `GITLAB_LOGGER_MINLEVEL`

### exclude (deprecated)

Exclude files based on their full path name (used when watching a directory). List of keywords, using a pipe (`|`) as separator. Implemented with `string.Contains`.

default: `sasl|config|lock|@|gzip|tgz|gz`

### complete-lines

Set to true to only emit comlete lines.

default: `true`

Equivalent environment variable: `GITLAB_LOGGER_COMPLETE_LINES`

### read-from-end

In a container environment, the lifetime of `gitlab-logger` is typically congruent with the existence and lifetime of the log files. Therefore, we want to read from the *start* of the files, in case there is some output from other processes before `gitlab-logger` was fully started. This is a safer default as no logs are lost.

Alternatively, `gitlab-logger` can be used in more long-lived environments. This means it may start up with historical logs that have been read already. You can use `read-from-end` to tell `gitlab-logger` to start reading from the **end** of the files, only capturing new log lines.

Equivalent environment variable: `GITLAB_LOGGER_READ_FROM_END`

### max-log-file-size

Maximum allowed log file size in bytes before the file is truncated.

default: `1000`

Equivalent environment variable: `GITLAB_LOGGER_MAX_FILESIZE`

### truncate-logs

In certain environments that do not contain a mechanism for log rotation, such
as in a container, the `truncate-logs` option will periodically truncate log
files that exceed the configured maximum size in `max-log-file-size` per the
interval defined in `truncate-logs-interval`.

default: `false`

Equivalent environment variable: `GITLAB_LOGGER_TRUNCATE_LOGS`

### truncate-logs-interval

Interval in seconds in which to truncate log files.

default: `300`

Equivalent environment variable: `GITLAB_LOGGER_TRUNCATE_INTERVAL`

## How to give it a try

### Sidecar

`docker-compose -f docker-compose.sidecar.yml build && docker-compose -f docker-compose.sidecar.yml up -d && docker logs -f gitlab-logger_logger_1`

### Tail dropin replacement

`docker-compose -f docker-compose.dropin.yml build && docker-compose -f docker-compose.dropin.yml up -d && docker logs -f gitlab-logger_web_1`

### Log truncation test

`docker-compose -f docker-compose.truncate-logs.yml build && docker-compose -f docker-compose.truncate-logs.yml up`

Confirm that the `truncate-logs-log-generator-1` exits with an exit code of `0`.

## Example output

```plaintext
$ docker logs -f gitlab-logger_logger_1 | jq
{
  "date": "2020-05-22T00:47:17Z",
  "component": "gitaly",
  "subcomponent": "current",
  "level": "error",
  "file": "/var/log/gitlab/gitaly/current",
  "message": {
    "error": "open /var/opt/gitlab/gitaly/gitaly.pid: no such file or directory",
    "level": "error",
    "msg": "find gitaly",
    "time": "2020-05-22T00:47:17Z",
    "wrapper": 466
  }
}
{
  "date": "2020-05-22T00:47:17Z",
  "component": "gitaly",
  "subcomponent": "current",
  "level": "warning",
  "file": "/var/log/gitlab/gitaly/current",
  "message": "time=\"2020-05-22T00:47:17Z\" level=warning msg=\"git path not configured. Using default path resolution\" resolvedPath=/opt/gitlab/embedded/bin/git"
}
{
  "date": "2020-05-22T00:47:18Z",
  "component": "gitaly",
  "subcomponent": "current",
  "level": "warning",
  "file": "/var/log/gitlab/gitaly/current",
  "message": {
    "level": "warning",
    "msg": "spawned",
    "supervisor.args": [
      "bundle",
      "exec",
      "bin/ruby-cd",
      "/var/opt/gitlab/gitaly",
      "/opt/gitlab/embedded/service/gitaly-ruby/bin/gitaly-ruby",
      "479",
      "/var/opt/gitlab/gitaly/internal_sockets/ruby.0"
    ],
    "supervisor.name": "gitaly-ruby.0",
    "supervisor.pid": 498,
    "time": "2020-05-22T00:47:17.676Z"
  }
}
{
  "date": "2020-05-22T00:47:18Z",
  "component": "gitaly",
  "subcomponent": "current",
  "level": "warning",
  "file": "/var/log/gitlab/gitaly/current",
  "message": {
    "level": "warning",
    "msg": "spawned",
    "supervisor.args": [
      "bundle",
      "exec",
      "bin/ruby-cd",
      "/var/opt/gitlab/gitaly",
      "/opt/gitlab/embedded/service/gitaly-ruby/bin/gitaly-ruby",
      "479",
      "/var/opt/gitlab/gitaly/internal_sockets/ruby.1"
    ],
    "supervisor.name": "gitaly-ruby.1",
    "supervisor.pid": 500,
    "time": "2020-05-22T00:47:17.676Z"
  }
}
{
  "date": "2020-05-22T00:47:42Z",
  "component": "gitlab-rails",
  "subcomponent": "gitlab-rails-db-migrate-2020-05-22-00-47-26",
  "level": "notice",
  "file": "/var/log/gitlab/gitlab-rails/gitlab-rails-db-migrate-2020-05-22-00-47-26.log",
  "message": "psql:/opt/gitlab/embedded/service/gitlab-rails/db/structure.sql:3: NOTICE:  extension \"plpgsql\" already exists, skipping"
}
{
  "date": "2020-05-22T00:47:42Z",
  "component": "gitlab-rails",
  "subcomponent": "gitlab-rails-db-migrate-2020-05-22-00-47-26",
  "level": "notice",
  "file": "/var/log/gitlab/gitlab-rails/gitlab-rails-db-migrate-2020-05-22-00-47-26.log",
  "message": "psql:/opt/gitlab/embedded/service/gitlab-rails/db/structure.sql:5: NOTICE:  extension \"pg_trgm\" already exists, skipping"
}
```

## Watch it

[![asciicast](https://asciinema.org/a/lx8RDAWAAaMFXZNbiaseBRkDL.svg)](https://asciinema.org/a/lx8RDAWAAaMFXZNbiaseBRkDL)
