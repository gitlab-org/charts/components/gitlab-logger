GIT               := $(shell command -v git)
BUILD_TIME        := $(shell date +"%Y%m%d.%H%M%S")
GO_LDFLAGS        := -X main.version=${LOGGER_VERSION} -X main.buildtime=${BUILD_TIME}
INSTALL           := install
LOGGER_VERSION    ?= $(shell ${GIT} describe 2>/dev/null | sed 's/^v//' || echo unknown)
PREFIX            ?= /usr/local
prefix            ?= ${PREFIX}
exec_prefix       ?= ${prefix}
bindir            ?= ${exec_prefix}/bin
INSTALL_DEST_DIR  ?= ${DESTDIR}${bindir}
GOCOVER_COBERTURA := github.com/boumenot/gocover-cobertura

run_go_tests = go test -v ./... ${test_options}

.PHONY: all
all: gitlab-logger

gitlab-logger: cmd/gitlab-logger/main.go
	go build -o "$@" -ldflags '${GO_LDFLAGS}' cmd/gitlab-logger/main.go

.PHONY: prepare-tests
ifdef TEST_WITH_COVERAGE
test_options=-coverprofile=coverage.txt -covermode count
run_go_tests += && go run github.com/boumenot/gocover-cobertura < coverage.txt > coverage.xml
prepare-tests:
	go get ${GOCOVER_COBERTURA}
else
prepare-tests:
endif

.PHONY: test
test: prepare-tests
	$(call run_go_tests)

.PHONY: install
install: gitlab-logger
	mkdir -p ${INSTALL_DEST_DIR}
	install gitlab-logger "${INSTALL_DEST_DIR}"

.PHONY: clean
clean:
	rm -f gitlab-logger coverage.txt coverage.xml
