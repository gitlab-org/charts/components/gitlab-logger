package logger

import (
	"fmt"
	"gitlab-org/cloud-native/gitlab-logger/internal/config"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	tailer "github.com/nxadm/tail"
	"github.com/radovskyb/watcher"
)

const (
	fileMode = iota
	directoryMode
	start
	stop
	stopAll
)

// Logger performs a tail -f on a set of files defined by the config.
type Logger struct {
	cfg *config.Config

	c chan *tailEvent
}

type tailEvent struct {
	Op   int
	File string
}

func createFilePerModeMap(paths []string) map[int][]string {
	filePerMode := make(map[int][]string)
	for _, f := range paths {
		if strings.HasPrefix(f, "/") {
			mode := fileMode
			if isDir(f) {
				mode = directoryMode
			}
			filePerMode[mode] = append(filePerMode[mode], f)
		}
	}
	return filePerMode
}

func filesInDir(dir string) []string {
	result := []string{}

	walkDirFunc := func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		result = append(result, path)

		return nil
	}

	if err := filepath.WalkDir(dir, walkDirFunc); err != nil {
		log.Printf("unable to walk directory %s: %v\n", dir, err)
	}

	return result
}

func isDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

// New creates a new Logger with the provided config.
func New(cfg *config.Config) *Logger {
	l := &Logger{cfg: cfg}
	l.c = make(chan *tailEvent)

	log.SetFlags(0)

	return l
}

// Start watches for changes in the configured directories and performs the tail -f.
func (l *Logger) Start(paths []string) {
	ff := createFilePerModeMap(paths)
	if l.cfg.PidToWatch > 0 {
		go l.watchPid(l.cfg.PidToWatch, l.c)
	}
	go l.tail()
	if v, ok := ff[directoryMode]; ok {
		go l.watch(v, l.c)
	}
	for _, f := range ff[fileMode] {
		l.c <- &tailEvent{Op: start, File: f}
	}

	if l.cfg.TruncateLogs {
		go l.periodicTruncate(ff)
	}
}

func (l *Logger) Stop() {
	l.c <- &tailEvent{Op: stopAll}
}

func (l *Logger) watchPid(pid int, c chan *tailEvent) {
	pidDir := fmt.Sprintf("/proc/%d", pid)
	if _, err := os.Stat(pidDir); os.IsNotExist(err) {
		c <- &tailEvent{Op: stopAll}
		return
	}
	w := watcher.New()
	if err := w.Add(pidDir); err != nil {
		log.Printf("Unable to watch PID dir %s: %v\n", pidDir, err)
	}

	w.FilterOps(watcher.Remove)
	go func() {
		for {
			select {
			case err := <-w.Error:
				if err == watcher.ErrWatchedFileDeleted {
					c <- &tailEvent{Op: stopAll}
				} else {
					panic(err)
				}
			case <-w.Closed:
				return
			}
		}
	}()
	if err := w.Start(l.cfg.WatchCycle); err != nil {
		panic(err)
	}
}

// nolint:gocognit
func (l *Logger) watch(dirs []string, c chan *tailEvent) {
	w := watcher.New()
	w.AddFilterHook(l.watchFilter)
	w.FilterOps(watcher.Create, watcher.Write, watcher.Remove)
	go func() {
		for {
			select {
			case event := <-w.Event:
				switch event.Op {
				case watcher.Create, watcher.Write:
					_, ok := l.cfg.Tails.Load(event.Path)
					if !ok {
						c <- &tailEvent{Op: start, File: event.Path}
					}
				case watcher.Remove:
					c <- &tailEvent{Op: stop, File: event.Path}
				default:
					panic(nil)
				}
			case err := <-w.Error:
				if err == watcher.ErrWatchedFileDeleted {
					log.Printf("warning: %v\n", err)
				} else {
					panic(err)
				}
			case <-w.Closed:
				return
			}
		}
	}()
	for _, d := range dirs {
		if err := w.AddRecursive(d); err != nil {
			panic(err)
		}
	}
	if err := w.Start(l.cfg.WatchCycle); err != nil {
		panic(err)
	}
}

func (l *Logger) watchFilter(info os.FileInfo, fullPath string) error {
	if info.IsDir() {
		return watcher.ErrSkip
	}
	for _, w := range l.cfg.Exclude {
		if strings.Contains(fullPath, w) {
			return watcher.ErrSkip
		}
	}
	return nil
}

// nolint:gocognit
func (l *Logger) tail() {
	for evt := range l.c {
		switch evt.Op {
		case start:
			filename := evt.File
			go func() {
				whence := io.SeekStart
				if l.cfg.ReadFromEnd {
					whence = io.SeekEnd
				}
				t, err := tailer.TailFile(filename, tailer.Config{CompleteLines: l.cfg.CompleteLines, Poll: l.cfg.Poll, Follow: true, ReOpen: true, Location: &tailer.SeekInfo{Whence: whence}, Logger: tailer.DiscardingLogger})
				if err != nil {
					panic(err)
				}
				l.cfg.Tails.Store(filename, t)
				for line := range t.Lines {
					l.logTo(t.Filename, line.Text)
				}
			}()
		case stop:
			v, ok := l.cfg.Tails.Load(evt.File)
			if ok {
				err := v.(*tailer.Tail).Stop()
				if err != nil {
					log.Printf("unable to stop tail %s: %v\n", evt.File, err)
				}
				l.cfg.Tails.Delete(evt.File)
			}
		case stopAll:
			return
		default:
			panic(nil)
		}
	}
}

func (l *Logger) logTo(file, message string) {
	if len(message) == 0 {
		return
	}
	j := isJSON(message)
	if l.cfg.JSON && !j {
		return
	}

	label := ""
	if l.cfg.MinLevel > 0 {
		level := 0
		level, label = detectLevel(message)
		if level < l.cfg.MinLevel {
			return
		}
	}
	var line string
	if !j {
		line = fmt.Sprintf("\"time\":\"%s\",\"message\":%s", time.Now().Format(time.RFC3339), strconv.Quote(message))
	} else {
		line = message[1 : len(message)-1]
	}
	component, subcomponent := components(file)

	if label == "" {
		log.Printf("{\"component\": \"%s\",\"subcomponent\":\"%s\",%s}\n", component, subcomponent, line)
	} else {
		log.Printf("{\"component\": \"%s\",\"subcomponent\":\"%s\",\"level\":\"%s\",%s}\n", component, subcomponent, label, line)
	}
}

// truncate periodically sends truncate events to keep log files small.
func (l *Logger) periodicTruncate(ff map[int][]string) {
	interval := time.Duration(l.cfg.TruncateLogsInterval) * time.Second

	for range time.Tick(interval) {
		// Truncate specified files.
		for _, f := range ff[fileMode] {
			err := truncateFile(f, l.cfg.MaxLogFileSize)
			if err != nil {
				log.Printf("unable to truncate file %s: %v\n", f, err)
			}
		}

		// Truncate specified directories.
		if dirs, ok := ff[directoryMode]; ok {
			for _, dir := range dirs {
				for _, f := range filesInDir(dir) {
					err := truncateFile(f, l.cfg.MaxLogFileSize)
					if err != nil {
						log.Printf("unable to truncate file %s in directory: %v\n", f, err)
					}
				}
			}
		}
	}
}

func truncateFile(file string, maxBytes int64) error {
	f, err := os.Stat(file)
	if err != nil {
		return err
	}

	if f.Size() <= maxBytes {
		return nil
	}

	if err := os.Truncate(file, 0); err != nil {
		return err
	}

	return nil
}

func detectLevel(message string) (int, string) {
	level := 100
	label := "unknown"
	message = strings.ToLower(message)
	debug := []string{"debug"}
	info := []string{"info", "log", "get", "post", "processing", "starting", "started", "completed", "success", "saving", "saved", "creating", "created"}
	notice := []string{"notice"}
	warn := []string{"warn"}
	err := []string{"error", "failed"}
	fatal := []string{"fatal", "emerg"}
	for _, w := range debug {
		if strings.Contains(message, w) {
			level = 1
			label = "debug"
			break
		}
	}
	for _, w := range info {
		if strings.Contains(message, w) {
			level = 2
			label = "info"
			break
		}
	}
	for _, w := range notice {
		if strings.Contains(message, w) {
			level = 3
			label = "notice"
			break
		}
	}
	for _, w := range warn {
		if strings.Contains(message, w) {
			level = 4
			label = "warning"
			break
		}
	}
	for _, w := range err {
		if strings.Contains(message, w) {
			level = 5
			label = "error"
			break
		}
	}
	for _, w := range fatal {
		if strings.Contains(message, w) {
			level = 6
			label = "fatal"
			break
		}
	}
	return level, label
}

func components(file string) (string, string) {
	parts := strings.Split(file, "/")
	component := parts[len(parts)-2:][0]
	subcomponent := parts[len(parts)-1:][0]
	subcomponent = strings.TrimSuffix(subcomponent, filepath.Ext(subcomponent))
	return component, subcomponent
}

func isJSON(s string) bool {
	// cheap detection; must be good / fastest for such logs parsing
	return s[:1] == "{" && s[len(s)-1:] == "}"
}
