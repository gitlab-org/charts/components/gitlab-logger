module gitlab-org/cloud-native/gitlab-logger

go 1.19

require (
	github.com/nxadm/tail v1.4.9-0.20211216163028-4472660a31a6
	github.com/radovskyb/watcher v1.0.7
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
